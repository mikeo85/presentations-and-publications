# Presentations and Publications

This repo contains and/or links to my publicly-available work, including presentations, publications, and professional blog posts.

## Index

### 2022

- **[Webinar: Defending Backups Against Ransomware - How to Prepare for Attackers Targeting Your Backups](https://youtu.be/enfmOHcJ2Cg)** - Webinar with Paul Sems

### 2021
- **[Last Line of Defense: Hardening Backup Systems Against Ransomware](./2021-TrustedSec-ISS-LastLineOfDefense-HardeningBackupSystemsAgainstRansomware.pdf)** - Presentation with Paul Sems at the 2021 Information Security Summit, Cleveland, OH.
- **[Data Loss Prevention in a Remote Work World.](https://www.trustedsec.com/events/webinar-data-loss-prevention-in-a-remote-work-world/)** - Webinar with Phil Rowland

### 2020
- [Blog Posts at Eagle Consulting Partners from 2017-2020](https://eagleconsultingpartners.com/author/mowens/)

### 2019
- [**Cybersecurity Guide for DD Board Superintentents**](2019-CybersecurityForSuperintendents-DDWhitePaper.pdf) ([Link](https://eagleconsultingpartners.com/dd-board-solutions/cybersecurity-guide-for-superintendents/))

## Copyright
&copy; Copyright Mike Owens and/or my employer at the time of content creation. Resources may be referenced or quoted with appropriate citation.

